import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  Query,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { TransformResponseInterceptor } from 'src/common/interceptor/transform_response.interceptor';
import { Product } from 'src/entity/product.entity';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  @UseInterceptors(TransformResponseInterceptor)
  create(@Body() createProductDto: CreateProductDto): Promise<Product> {
    return this.productService.create(createProductDto);
  }

  @Get()
  @UseInterceptors(TransformResponseInterceptor)
  findAll(@Query() page: number, @Query() limit: number): Promise<Product[]> {
    return this.productService.findAll(+page, +limit);
  }

  @Get(':id')
  @UseInterceptors(TransformResponseInterceptor)
  findOne(@Param('id') id: number): Promise<Product> {
    return this.productService.findOne(+id);
  }

  @Patch(':id')
  @UseInterceptors(TransformResponseInterceptor)
  update(
    @Param('id') id: number,
    @Body() updateProductDto: UpdateProductDto,
  ): Promise<Product> {
    return this.productService.update(+id, updateProductDto);
  }

  @Delete(':id')
  @UseInterceptors(TransformResponseInterceptor)
  remove(@Param('id') id: number): Promise<void> {
    return this.productService.remove(+id);
  }
}
