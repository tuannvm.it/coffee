import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/entity/product.entity';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    return await this.productRepository.save(createProductDto);
  }

  async findAll(
    offset = 0,
    limit: number = Number.MAX_SAFE_INTEGER,
  ): Promise<Product[]> {
    return await this.productRepository.find({ skip: offset, take: limit });
  }

  async findOne(id: number): Promise<Product> {
    return await this.productRepository.findOne(id);
  }

  async update(
    id: number,
    updateProductDto: UpdateProductDto,
  ): Promise<Product> {
    const updateProduct = await this.productRepository
      .createQueryBuilder()
      .update(Product, { ...updateProductDto })
      .where('product.id = :id', { id })
      .returning('*')
      .updateEntity(true)
      .execute();
    return updateProduct?.raw[0] || null;
  }

  async remove(id: number) {
    await this.productRepository.softDelete(id);
  }
}
