import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { OrderLine } from './order_line.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column('varchar', { length: 255 })
  name: string;

  @Column('varchar', { nullable: true, length: 255 })
  img_path: string;

  @Column('float')
  price: number;

  @OneToMany(() => OrderLine, (order_line) => order_line.product_id)
  order_line: OrderLine[];

  @CreateDateColumn()
  create_at: Date;

  @UpdateDateColumn()
  update_at: Date;

  @DeleteDateColumn()
  delete_at: Date;
}
