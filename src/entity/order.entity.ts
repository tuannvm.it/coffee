import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { OrderLine } from './order_line.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToMany(() => OrderLine, (orderline) => orderline.order_id)
  public order_line!: OrderLine[];

  @Column('float')
  amount: number;

  @CreateDateColumn()
  create_at: Date;

  @UpdateDateColumn()
  update_at: Date;

  @DeleteDateColumn()
  delete_at: Date;
}
