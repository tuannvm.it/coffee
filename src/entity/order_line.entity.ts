import {
  Entity,
  Column,
  ManyToOne,
  PrimaryGeneratedColumn,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { Order } from './order.entity';
import { Product } from './product.entity';

@Entity()
export class OrderLine {
  @PrimaryGeneratedColumn()
  public id!: number;

  @ManyToOne(() => Order, (order) => order.order_line)
  @JoinColumn({ name: 'order_id' })
  public order_id!: Order;

  @ManyToOne(() => Product, (product) => product.order_line)
  @JoinColumn({ name: 'product_id' })
  public product_id!: Product;

  @Column('int')
  quantity: number;

  @Column('float')
  amount: number;

  @CreateDateColumn()
  create_at: Date;

  @UpdateDateColumn()
  update_at: Date;

  @DeleteDateColumn()
  delete_at: Date;
}
